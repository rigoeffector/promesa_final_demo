<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(isset($_GET['action']) && $_GET['action']=="add"){
	$id=intval($_GET['id']);
	if(isset($_SESSION['cart'][$id])){
		$_SESSION['cart'][$id]['quantity']++;
	}else{
		$sql_p="SELECT * FROM products WHERE id={$id}";
		$query_p=mysqli_query($bd,$sql_p);
		if(mysqli_num_rows($query_p)!=0){
			$row_p=mysqli_fetch_array($query_p);
			$_SESSION['cart'][$row_p['id']]=array("quantity" => 1, "price" => $row_p['productPrice']);
			header('location:index.php');
		}else{
			$message="Product ID is invalid";
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Meta -->
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">
	    <meta name="keywords" content="MediaCenter, Template, eCommerce">
	    <meta name="robots" content="all">

	    <title>Promes e-Store</title>

	    <!-- Bootstrap Core CSS -->

	    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

	    <!-- Customizable CSS -->
	    <link rel="stylesheet" href="assets/css/main.css">
	    <link rel="stylesheet" href="assets/css/green.css">
	    <link rel="stylesheet" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" href="assets/css/owl.transitions.css">
		<!--<link rel="stylesheet" href="assets/css/owl.theme.css">-->
		<link href="assets/css/lightbox.css" rel="stylesheet">
		<link rel="stylesheet" href="assets/css/animate.min.css">
		<link rel="stylesheet" href="css/claudeadmin.css">
		<link rel="stylesheet" href="assets/css/rateit.css">
		<link rel="stylesheet" href="assets/css/bootstrap-select.min.css">

		<!-- Demo Purpose Only. Should be removed in production -->
		<link rel="stylesheet" href="assets/css/config.css">

		<link href="assets/css/green.css" rel="alternate stylesheet" title="Green color">
		<link href="assets/css/blue.css" rel="alternate stylesheet" title="Blue color">
		<link href="assets/css/red.css" rel="alternate stylesheet" title="Red color">
		<link href="assets/css/orange.css" rel="alternate stylesheet" title="Orange color">
		<link href="assets/css/dark-green.css" rel="alternate stylesheet" title="Darkgreen color">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>

	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Ion Icon Fonts-->
	<link rel="stylesheet" href="css/ionicons.min.css">
	<!-- Bootstrap  -->


	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Flexslider  -->
	<link rel="stylesheet" href="css/flexslider.css">

	<!-- Owl Carousel -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Date Picker -->
	<link rel="stylesheet" href="css/bootstrap-datepicker.css">
	<!-- Flaticons  -->
	<link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

	<!-- Theme style  -->
	<!-- <link rel="stylesheet" href="css/style.css"> -->
		<!-- Favicon -->
		<link rel="shortcut icon" href="./Images/Logo/Promesa e-Store Logo.png">

	</head>

		<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1">
<?php include('includes/top-header.php');?>
<?php include('includes/main-header.php');?>
<?php include('includes/menu-bar.php');?>
</header>

<!-- ============================================== HEADER : END ============================================== -->


<div class="body-content outer-top-xs" id="top-banner-and-menu">
	<!-- <div class="container"> -->
		
<!-- ============================================== TABS ============================================== -->




<div class="colorlib-intro">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 text-center">
						<h2 class="intro">It started with a simple idea: Create quality, Spread out the Good Service to all fellow Rwandans.</h2>
					</div>
				</div>
			</div>
		</div>




		<!-- ============================================== SCROLL TABS ============================================== -->
		<div id="product-tabs-slider" class="scroll-tabs inner-bottom-vs  wow fadeInUp">
			<div class="more-info-tab clearfix">
			</div>
			<div class="body-content outer-top-xs">
				<div class='container'>
					<div class='row single-product outer-bottom-sm '>




</div>
</div>
			<div class="col-md-12 col-sm-12" >
			<div class="tab-content outer-top-xs" >
				<div class="tab-pane in active" id="all">
					<center><h1>MEN'S BEST COLLECTION</h1></center>
					<div class="product-slider">
						<div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="4" >
						<?php
						$ret=mysqli_query($bd,"select * from products where category = 15  order by id desc ");
						while ($row=mysqli_fetch_array($ret))
						{
						?>
						</div>
<div class="container">
		<div class="col-md-9 pros" style="margin-top:20px;">
			<div class="products" style="
			text-align:center;">

	<div class="product">
		<div class="product-image" >
			<div class="image" >
				<a href="product-details.php?pid=<?php echo htmlentities($row['id']);?>">
				<img  src="admin/productimages/
				<?php echo htmlentities($row['productName']);?>/
				<?php echo htmlentities($row['productImage1']);?>" data-echo="admin/productimages/
				<?php echo htmlentities($row['productName']);?> /
				<?php echo htmlentities($row['productImage1']);?>"  ></a>
			</div><!-- /.image -->


		</div><!-- /.product-image -->

		<div class="col-md-12 col-sm-12">
		<div class="product-info text-left">
			<h3 class="name" style="text-align:center; color:#339966;">
				<a href="product-details.php?pid=<?php echo htmlentities($row['id']);?>">
			<?php echo htmlentities($row['productName']);?></a></h3>



			<div class="product-price" style="text-align:center;">
				<span style="color:rgb(83, 135, 180);" class="price">
					<?php echo htmlentities($row['productPrice']);?>&nbsp;&nbsp;Rwf</span>
					<!-- <span class="price-before-discount">RWF.
						<?php echo htmlentities($row['productPriceBeforeDiscount']);?> Discount	</span> -->

			</div><!-- /.product-price -->

		</div><!-- /.product-info -->
					<div class="action">
						<a  href="index.php?page=product&action=add&id=<?php echo $row['id']; ?>"
						 id="customizedbtn">Add To Cart</a>
					 </div>
					 <!-- <div class="col-sm-7 whish"> -->
					 <?php
						// COde for Wishlist
						if(isset($_GET['pid']) && $_GET['action']=="wishlist" ){
							if(strlen($_SESSION['login'])==0)
								{
						header('location:login.php');
						}
						else
						{
						mysqli_query("insert into wishlist(userId,productId) values('".$_SESSION['id']."','".$_GET['pid']."')");
						echo "<script>alert('Product aaded in wishlist');</script>";
						header('location:my-wishlist.php');

						}
						}
						?>
						 <li class="lnk wishlist" style="list-style:none;">
							 <a class="add-to-cart" href="category.php?pid=<?php echo htmlentities($row['id'])?>&&action=wishlist" title="Wishlist">
									&nbsp;&nbsp;&nbsp;&nbsp;<i class="icon fa fa-heart whish">Add To Wishlist</i>
							 </a>
						 </li>
				 <!-- </div> -->

			</div><!-- /.product -->

			</div><!-- /.products -->
		</div><!-- /.item -->
	<?php } ?>
		</div>
		</div><!-- /.home-owl-carousel -->
		</div><!-- /.product-slider -->
		</div>

</div>
</div>


<center><div class="all_Shop">
	<button>Shop All</button>
</div>
</center>

    <!-- ============================================== TABS ============================================== -->

		<!-- ============================================== TABS : END ============================================== -->

</div>
</div>

<?php include('includes/footer.php');?>

	<script src="assets/js/jquery-1.11.1.min.js"></script>

	<script src="assets/js/bootstrap.min.js"></script>

	<script src="assets/js/bootstrap-hover-dropdown.min.js"></script>
	<script src="assets/js/owl.carousel.min.js"></script>

	<script src="assets/js/echo.min.js"></script>
	<script src="assets/js/jquery.easing-1.3.min.js"></script>
	<script src="assets/js/bootstrap-slider.min.js"></script>
    <script src="assets/js/jquery.rateit.min.js"></script>
    <script type="text/javascript" src="assets/js/lightbox.min.js"></script>
    <script src="assets/js/bootstrap-select.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
	<script src="assets/js/scripts.js"></script>

	<!-- For demo purposes – can be removed on production -->

	<script src="switchstylesheet/switchstylesheet.js"></script>

	<script>
		$(document).ready(function(){
			$(".changecolor").switchstylesheet( { seperator:"color"} );
			$('.show-theme-options').click(function(){
				$(this).parent().toggleClass('open');
				return false;
			});
		});

		$(window).bind("load", function() {
		   $('.show-theme-options').delay(500).trigger('click');
		});
	</script>
	<!-- For demo purposes – can be removed on production : End -->



</body>
</html>
