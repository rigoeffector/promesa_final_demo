-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 05, 2018 at 10:22 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shopping`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updationDate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `creationDate`, `updationDate`) VALUES
(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '2017-01-24 16:21:18', '05-06-2018 01:31:05 PM');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `categoryName` varchar(255) NOT NULL,
  `categoryDescription` longtext NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updationDate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `categoryName`, `categoryDescription`, `creationDate`, `updationDate`) VALUES
(8, 'Electronic Devices', 'no description', '2018-05-30 12:25:26', '04-09-2018 01:06:04 AM'),
(9, 'Men', '', '2018-06-05 07:28:11', '04-09-2018 01:06:19 AM'),
(10, 'Women', 'Womes Style\r\n', '2018-06-05 07:34:44', '04-09-2018 01:08:07 AM');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `userId` int(11) NOT NULL,
  `userName` varchar(30) NOT NULL,
  `userEmail` varchar(60) NOT NULL,
  `userPass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`userId`, `userName`, `userEmail`, `userPass`) VALUES
(1, 'didos', 'clients@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(2, 'clients', 'clients@gmail.comm', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(4, 'Marie Rose Kigali', 'mrosekigali@gmail.com', '65e84be33532fb784c48129675f9eff3a682b27168c0ea744b2cf58ee02337c5'),
(5, 'kigali', 'kigali@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(6, 'didos', 'didos@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(7, 'umucuruzi', 'umucuruzi@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(9, 'Didos', 'didos@gmail.comcc', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(11, 'umukobwa', 'umukobwa@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(12, 'trader', 'trader@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92'),
(13, 'annet', 'annet@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `productId` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `orderDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `paymentMethod` varchar(50) DEFAULT NULL,
  `orderStatus` varchar(55) DEFAULT NULL,
  `number` varchar(30) NOT NULL,
  `paymentapproval` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `userId`, `productId`, `quantity`, `orderDate`, `paymentMethod`, `orderStatus`, `number`, `paymentapproval`) VALUES
(22, 7, '33', 10, '2018-06-02 11:44:00', 'Tigo cash', 'Delivered', '072222222222222222222', 'Paid'),
(23, 7, '41', 1, '2018-06-04 07:53:01', NULL, NULL, '', ''),
(24, 7, '47', 1, '2018-06-04 07:53:01', NULL, NULL, '', ''),
(25, 7, '41', 1, '2018-06-04 07:57:03', NULL, NULL, '', ''),
(26, 7, '47', 1, '2018-06-04 07:57:03', NULL, NULL, '', ''),
(27, 9, '53', 10, '2018-06-05 07:38:47', 'Airtel money', 'Delivered', '073989898', 'Paid'),
(28, 9, '52', 1, '2018-06-05 08:11:09', 'Tigo cash', NULL, '078787', ''),
(29, 9, '53', 1, '2018-06-05 08:37:09', 'Tigo cash', NULL, '072222222', ''),
(30, 9, '54', 20, '2018-06-05 09:24:39', 'Tigo cash', 'Delivered', '072222222', 'Paid'),
(31, 11, '55', 3, '2018-06-06 11:39:36', NULL, NULL, '', ''),
(32, 11, '55', 3, '2018-06-06 13:02:42', NULL, NULL, '', ''),
(33, 11, '55', 3, '2018-06-06 13:39:22', NULL, NULL, '', ''),
(34, 11, '53', 60, '2018-06-06 13:40:43', NULL, NULL, '', ''),
(35, 3, '54', 3, '2018-06-07 13:04:45', 'Tigo cash', NULL, '0724323457', ''),
(36, 14, '33', 1, '2018-06-10 19:22:43', 'Tigo cash', NULL, '0725438172', ''),
(37, 1, '1', 1, '2018-06-11 19:20:12', 'Debit / Credit card', NULL, '4', ''),
(38, 15, '1', 20, '2018-06-11 19:23:44', 'Tigo cash', 'Delivered', '0725433212', 'Paid'),
(39, 16, '3', 10, '2018-06-15 21:36:50', 'Tigo cash', 'Delivered', '072345132', 'Paid'),
(40, 3, '5', 20, '2018-09-03 20:05:06', NULL, 'in Process', '', 'Paid'),
(41, 3, '5', 1, '2018-09-04 10:36:29', NULL, NULL, '', ''),
(42, 3, '5', 1, '2018-09-04 10:42:41', NULL, NULL, '', ''),
(43, 1, '4', 1, '2018-09-04 16:07:35', 'Debit / Credit card', NULL, '4', ''),
(44, 1, '5', 1, '2018-09-04 16:07:35', 'Debit / Credit card', NULL, '4', ''),
(45, 1, '5', 1, '2018-09-04 16:22:50', NULL, NULL, '', ''),
(46, 1, '6', 1, '2018-09-04 16:42:55', NULL, NULL, '', ''),
(47, 3, '6', 1, '2018-09-04 16:44:59', NULL, NULL, '', ''),
(48, 1, '6', 2, '2018-09-04 16:51:05', NULL, NULL, '', ''),
(49, 17, '6', 1, '2018-09-04 17:03:47', 'Airtel money', 'Delivered', '789654123', 'Paid'),
(50, 17, '6', 1, '2018-09-04 17:04:19', 'Airtel money', NULL, '789654123', ''),
(51, 18, '7', 1, '2018-09-04 20:38:46', 'MTN Mobile money', NULL, '7854', ''),
(52, 18, '7', 1, '2018-09-04 20:43:18', 'Airtel money', NULL, '7', ''),
(53, 18, '7', 1, '2018-09-04 20:43:42', 'Airtel money', NULL, '7', ''),
(54, 18, '6', 1, '2018-09-04 21:08:30', 'MTN Mobile money', NULL, '45454545', '');

-- --------------------------------------------------------

--
-- Table structure for table `ordertrackhistory`
--

CREATE TABLE `ordertrackhistory` (
  `id` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `remark` mediumtext NOT NULL,
  `postingDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `payment` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ordertrackhistory`
--

INSERT INTO `ordertrackhistory` (`id`, `orderId`, `status`, `remark`, `postingDate`, `payment`) VALUES
(1, 22, 'in Process', 'npo remARKS', '2018-06-02 11:44:49', 'Paid'),
(2, 22, 'Delivered', 'YES DEAL DONE', '2018-06-02 11:45:27', 'Paid'),
(3, 27, 'in Process', 'tegereza tugiye kukuzanira ibyo wasabye', '2018-06-05 07:48:16', 'Paid'),
(4, 27, 'Delivered', 'dea l dome', '2018-06-05 07:50:56', 'Paid'),
(5, 30, 'in Process', 'biraje gato', '2018-06-05 09:25:24', 'Paid'),
(6, 30, 'Delivered', 'deal done', '2018-06-05 09:25:43', 'Paid'),
(7, 38, 'in Process', 'tegereza biri muznizra', '2018-06-11 19:25:14', 'Paid'),
(8, 38, 'Delivered', 'deal done', '2018-06-11 19:25:38', 'Paid'),
(9, 39, 'in Process', 'Kiri munizra tegereza', '2018-06-15 21:38:35', 'Paid'),
(10, 39, 'Delivered', 'byarangiye', '2018-06-15 21:38:56', 'Paid'),
(11, 40, 'in Process', 'Soon As Possible They Come There', '2018-09-03 20:14:32', 'Paid'),
(12, 49, 'Delivered', 'Okay Pretty cool to buy with us', '2018-09-04 20:54:38', 'Paid');

-- --------------------------------------------------------

--
-- Table structure for table `productreviews`
--

CREATE TABLE `productreviews` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `quality` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `summary` varchar(255) NOT NULL,
  `review` longtext NOT NULL,
  `reviewDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `productreviews`
--

INSERT INTO `productreviews` (`id`, `productId`, `quality`, `price`, `value`, `name`, `summary`, `review`, `reviewDate`) VALUES
(1, 7, 5, 3, 1, 'rigobert', 'Pretty Cool Ecouteur', 'Nice one Ever', '2018-09-04 19:25:31'),
(2, 7, 5, 3, 1, 'rigobert', 'Pretty Cool Ecouteur', 'Nice one Ever', '2018-09-04 19:26:25'),
(3, 7, 5, 3, 1, 'rigobert', 'Pretty Cool Ecouteur', 'Nice one Ever', '2018-09-04 19:27:38'),
(4, 7, 5, 3, 1, 'rigobert', 'Pretty Cool Ecouteur', 'Nice one Ever', '2018-09-04 19:28:37'),
(5, 7, 5, 3, 1, 'rigobert', 'Pretty Cool Ecouteur', 'Nice one Ever', '2018-09-04 19:28:51'),
(6, 7, 5, 3, 1, 'rigobert', 'Pretty Cool Ecouteur', 'Nice one Ever', '2018-09-04 19:29:19'),
(7, 7, 5, 3, 1, 'rigobert', 'Pretty Cool Ecouteur', 'Nice one Ever', '2018-09-04 19:30:02'),
(8, 7, 5, 3, 1, 'rigobert', 'Pretty Cool Ecouteur', 'Nice one Ever', '2018-09-04 19:30:40'),
(9, 7, 5, 3, 1, 'rigobert', 'Pretty Cool Ecouteur', 'Nice one Ever', '2018-09-04 19:31:02'),
(10, 7, 5, 3, 1, 'rigobert', 'Pretty Cool Ecouteur', 'Nice one Ever', '2018-09-04 19:31:31'),
(11, 7, 5, 3, 1, 'rigobert', 'Pretty Cool Ecouteur', 'Nice one Ever', '2018-09-04 19:34:22'),
(12, 7, 5, 3, 1, 'rigobert', 'Pretty Cool Ecouteur', 'Nice one Ever', '2018-09-04 19:35:05'),
(13, 7, 5, 3, 1, 'rigobert', 'Pretty Cool Ecouteur', 'Nice one Ever', '2018-09-04 19:35:21'),
(14, 7, 5, 3, 1, 'rigobert', 'Pretty Cool Ecouteur', 'Nice one Ever', '2018-09-04 19:36:03'),
(15, 7, 5, 3, 1, 'rigobert', 'Pretty Cool Ecouteur', 'Nice one Ever', '2018-09-04 19:36:43');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `subCategory` int(11) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `productCompany` varchar(255) NOT NULL,
  `phone` varchar(400) NOT NULL,
  `productPrice` int(11) NOT NULL,
  `productPriceBeforeDiscount` int(11) NOT NULL,
  `productDescription` longtext NOT NULL,
  `productImage1` varchar(255) NOT NULL,
  `productImage2` varchar(255) NOT NULL,
  `productImage3` varchar(255) NOT NULL,
  `shippingCharge` int(11) NOT NULL,
  `productAvailability` varchar(255) NOT NULL,
  `postingDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updationDate` varchar(255) NOT NULL,
  `status` varchar(20) NOT NULL,
  `quantity` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category`, `subCategory`, `productName`, `productCompany`, `phone`, `productPrice`, `productPriceBeforeDiscount`, `productDescription`, `productImage1`, `productImage2`, `productImage3`, `shippingCharge`, `productAvailability`, `postingDate`, `updationDate`, `status`, `quantity`) VALUES
(6, 8, 36, 'Infinx Charger', 'Tecno SmartPhone', '0784521036', 3000, 0, 'Faster Charger', 'infinix_charger.png', 'headphones.jpg', 'iphone3GS_Accessories.jpg', 1500, 'In Stock', '2018-09-04 16:40:39', '', 'yes', '14'),
(7, 8, 36, 'Ecouteur', 'Kigali Heights ', '078452013', 3000, 0, 'Bass X', 'mobile_headphones.jpg', 'stand_holder.jpg', 'selfiestick_pochete.jpg', 50, 'Out of Stock', '2018-09-04 18:48:06', '', 'yes', '97');

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `id` int(11) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `subcategory` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updationDate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`id`, `categoryid`, `subcategory`, `creationDate`, `updationDate`) VALUES
(36, 8, 'Smart Phones', '2018-05-30 12:26:00', '04-09-2018 01:07:10 AM'),
(37, 9, 'Shirts', '2018-06-05 07:28:42', '04-09-2018 01:07:26 AM'),
(38, 9, 'Suits', '2018-06-05 09:23:22', '04-09-2018 01:08:35 AM');

-- --------------------------------------------------------

--
-- Table structure for table `userlog`
--

CREATE TABLE `userlog` (
  `id` int(11) NOT NULL,
  `userEmail` varchar(255) NOT NULL,
  `userip` binary(16) NOT NULL,
  `loginTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logout` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userlog`
--

INSERT INTO `userlog` (`id`, `userEmail`, `userip`, `loginTime`, `logout`, `status`) VALUES
(1, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-02-26 11:18:50', '', 1),
(2, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-02-26 11:29:33', '', 1),
(3, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-02-26 11:30:11', '', 1),
(4, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-02-26 15:00:23', '26-02-2017 11:12:06 PM', 1),
(5, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-02-26 18:08:58', '', 0),
(6, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-02-26 18:09:41', '', 0),
(7, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-02-26 18:10:04', '', 0),
(8, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-02-26 18:10:31', '', 0),
(9, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-02-26 18:13:43', '', 1),
(10, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-02-27 18:52:58', '', 0),
(11, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-02-27 18:53:07', '', 1),
(12, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-03-03 18:00:09', '', 0),
(13, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-03-03 18:00:15', '', 1),
(14, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-03-06 18:10:26', '', 1),
(15, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-03-07 12:28:16', '', 1),
(16, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-03-07 18:43:27', '', 1),
(17, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-03-07 18:55:33', '', 1),
(18, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-03-07 19:44:29', '', 1),
(19, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-03-08 19:21:15', '', 1),
(20, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-03-15 17:19:38', '', 1),
(21, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-03-15 17:20:36', '15-03-2017 10:50:39 PM', 1),
(22, 'anuj.lpu1@gmail.com', 0x3a3a3100000000000000000000000000, '2017-03-16 01:13:57', '', 1),
(23, 'didos@gmail.com', 0x3a3a3100000000000000000000000000, '2017-10-27 07:59:56', '27-10-2017 01:30:28 PM', 1),
(24, 'didier@gmail.com', 0x3a3a3100000000000000000000000000, '2017-10-27 10:38:26', '', 0),
(25, 'didos@gmail.com', 0x3a3a3100000000000000000000000000, '2017-10-27 10:38:38', '', 1),
(26, 'sonia@gmail.com', 0x3a3a3100000000000000000000000000, '2017-10-27 13:05:52', '27-10-2017 06:40:08 PM', 1),
(27, 'sonia@gmail.com', 0x3a3a3100000000000000000000000000, '2017-10-27 13:10:26', '', 1),
(28, 'sonia@gmail.com', 0x3a3a3100000000000000000000000000, '2017-10-27 13:10:40', '27-10-2017 06:40:48 PM', 0),
(29, 'sonia@gmail.com', 0x3a3a3100000000000000000000000000, '2017-10-27 13:10:57', '27-10-2017 06:41:20 PM', 1),
(30, 'sonia@gmail.com', 0x3a3a3100000000000000000000000000, '2017-10-27 13:29:08', '27-10-2017 07:02:23 PM', 1),
(31, 'sonia@gmail.com', 0x3a3a3100000000000000000000000000, '2017-11-06 08:52:10', '06-11-2017 02:22:25 PM', 1),
(32, 'sonia@gmail.com', 0x3a3a3100000000000000000000000000, '2017-11-06 08:57:13', '06-11-2017 02:28:01 PM', 1),
(33, 'didier@gmail.com', 0x3a3a3100000000000000000000000000, '2017-11-15 09:15:53', '15-11-2017 02:46:12 PM', 1),
(34, 'sonia@gmail.com', 0x3a3a3100000000000000000000000000, '2017-11-22 09:44:05', '', 1),
(35, 'sonia@gmail.com', 0x3a3a3100000000000000000000000000, '2017-11-22 09:58:10', '', 1),
(36, 'sonia@gmail.com', 0x3a3a3100000000000000000000000000, '2017-12-08 10:24:17', '', 1),
(37, 'didos250@outlook.com', 0x3a3a3100000000000000000000000000, '2017-12-19 12:18:23', '', 0),
(38, 'sonia@gmail.com', 0x3a3a3100000000000000000000000000, '2017-12-19 12:18:31', '', 1),
(39, 'didos250@outlook.com', 0x3a3a3100000000000000000000000000, '2018-01-29 11:49:52', '', 0),
(40, 'didos250@outlook.com', 0x3a3a3100000000000000000000000000, '2018-01-29 11:49:52', '', 0),
(41, 'didos@gmail.com', 0x3a3a3100000000000000000000000000, '2018-01-29 11:50:30', '29-01-2018 07:17:49 PM', 1),
(42, 'didos@gmail.com', 0x3a3a3100000000000000000000000000, '2018-01-31 11:08:59', '', 1),
(43, 'didos@gmail.com', 0x3a3a3100000000000000000000000000, '2018-02-02 10:33:03', '', 1),
(44, 'didos@gmail.com', 0x3a3a3100000000000000000000000000, '2018-02-08 10:49:05', '', 1),
(45, 'jackson@gmail.com', 0x3a3a3100000000000000000000000000, '2018-05-22 12:39:24', '', 1),
(46, 'vivens@gmail.com', 0x3a3a3100000000000000000000000000, '2018-05-22 15:26:19', '', 0),
(47, 'vivens@gmail.com', 0x3a3a3100000000000000000000000000, '2018-05-22 15:26:30', '22-05-2018 09:14:36 PM', 1),
(48, 'user@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-02 11:42:06', '', 0),
(49, 'jackson@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-02 11:42:14', '02-06-2018 05:40:02 PM', 1),
(50, 'jackson@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-04 10:32:06', '', 1),
(51, 'kiyovu@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-05 07:38:22', '', 0),
(52, 'kiyovu@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-05 07:38:34', '', 1),
(53, 'kiyovu@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-05 08:10:18', '05-06-2018 01:44:20 PM', 1),
(54, 'kiyovu@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-05 08:28:44', '', 1),
(55, 'kiyovu@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-05 09:24:36', '', 1),
(56, 'rose@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-06 11:32:26', '', 0),
(57, 'rose@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-06 11:32:35', '', 0),
(58, 'rosemuka@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-06 11:32:41', '', 0),
(59, 'shopping@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-06 11:36:34', '', 1),
(60, 'shopping@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-06 13:02:36', '', 1),
(61, 'rose@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-07 13:02:20', '', 0),
(62, 'rosemuka@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-07 13:02:26', '', 0),
(63, 'trader@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-07 13:02:35', '', 0),
(64, 'adiel@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-07 13:03:12', '', 0),
(65, 'didos@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-07 13:04:13', '', 1),
(66, 'emeri@gmail.com', 0x3139322e3136382e34332e3100000000, '2018-06-10 19:22:12', '', 1),
(67, 'umuguzi@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-11 19:22:17', '', 0),
(68, 'umuguzui@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-11 19:23:01', '', 1),
(69, 'annet@gmail.com', 0x3a3a3100000000000000000000000000, '2018-06-15 21:36:30', '', 1),
(70, 'claude@gmail.com', 0x3a3a3100000000000000000000000000, '2018-09-03 20:04:34', '', 0),
(71, 'didos@gmail.com', 0x3a3a3100000000000000000000000000, '2018-09-03 20:04:58', '', 1),
(72, 'didos@gmail.com', 0x3a3a3100000000000000000000000000, '2018-09-03 20:15:38', '04-09-2018 02:02:18 AM', 1),
(73, 'didos@gmail.com', 0x3a3a3100000000000000000000000000, '2018-09-04 10:36:23', '04-09-2018 10:13:34 PM', 1),
(74, 'didos@gmail.com', 0x3a3a3100000000000000000000000000, '2018-09-04 16:43:57', '04-09-2018 10:14:10 PM', 1),
(75, 'didos@gmail.com', 0x3a3a3100000000000000000000000000, '2018-09-04 16:44:44', '', 1),
(76, 'didos@gmail.com', 0x3a3a3100000000000000000000000000, '2018-09-04 17:01:54', '', 0),
(77, 'didos@gmail.com', 0x3a3a3100000000000000000000000000, '2018-09-04 17:02:10', '', 0),
(78, 'rigobert@gmail.com', 0x3a3a3100000000000000000000000000, '2018-09-04 17:03:14', '', 1),
(79, 'rigobert@gmail.com', 0x3a3a3100000000000000000000000000, '2018-09-04 20:31:14', '', 0),
(80, 'rigorene48@gmail.com', 0x3a3a3100000000000000000000000000, '2018-09-04 20:32:19', '', 0),
(81, 'didos@gmail.com', 0x3a3a3100000000000000000000000000, '2018-09-04 20:33:24', '', 0),
(82, 'vava@gmail.com', 0x3a3a3100000000000000000000000000, '2018-09-04 20:35:21', '', 1),
(83, 'vava@gmail.com', 0x3a3a3100000000000000000000000000, '2018-09-04 20:56:02', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `contactno` bigint(11) NOT NULL,
  `password` varchar(255) NOT NULL,
  `shippingAddress` longtext NOT NULL,
  `shippingState` varchar(255) NOT NULL,
  `shippingCity` varchar(255) NOT NULL,
  `shippingPincode` int(11) NOT NULL,
  `billingAddress` longtext NOT NULL,
  `billingState` varchar(255) NOT NULL,
  `billingCity` varchar(255) NOT NULL,
  `billingPincode` int(11) NOT NULL,
  `regDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updationDate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `contactno`, `password`, `shippingAddress`, `shippingState`, `shippingCity`, `shippingPincode`, `billingAddress`, `billingState`, `billingCity`, `billingPincode`, `regDate`, `updationDate`) VALUES
(9, 'hotel kiyovu', 'kiyovu@gmail.com', 787878, 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 0, '', '', '', 0, '2018-06-05 07:38:12', ''),
(10, 'didos', 'didos20@outlook.comm', 9223372036854775807, 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 0, '', '', '', 0, '2018-06-05 13:58:20', ''),
(11, 'shopping', 'shopping@gmail.com', 788888888, 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 0, '', '', '', 0, '2018-06-06 11:36:25', ''),
(12, 'fdfdf', 'fdfdfdf@hghgh.com', 781259031, 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 0, '', '', '', 0, '2018-06-07 13:01:44', ''),
(13, 'adiel', 'asdiel@gmail.com', 787898765, 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 0, '', '', '', 0, '2018-06-07 13:03:01', ''),
(14, 'Emeri', 'emeri@gmail.com', 784561233, 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 0, '', '', '', 0, '2018-06-10 19:21:38', ''),
(15, 'umuguzi', 'umuguzui@gmail.com', 781945322, 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 0, '', '', '', 0, '2018-06-11 19:22:06', ''),
(16, 'annetcompany', 'annet@gmail.com', 789677645, 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 0, '', '', '', 0, '2018-06-15 21:36:19', ''),
(17, 'rigobert', 'rigobert@gmail.com', 781122334, 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 0, '', '', '', 0, '2018-09-04 17:03:01', ''),
(18, 'vava', 'vava@gmail.com', 723456789, 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 0, '', '', '', 0, '2018-09-04 20:35:04', '');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `postingDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`id`, `userId`, `productId`, `postingDate`) VALUES
(1, 9, 52, '2018-06-05 08:10:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`userId`),
  ADD UNIQUE KEY `userEmail` (`userEmail`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordertrackhistory`
--
ALTER TABLE `ordertrackhistory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orderId` (`orderId`);

--
-- Indexes for table `productreviews`
--
ALTER TABLE `productreviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productId` (`productId`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categoryid` (`categoryid`);

--
-- Indexes for table `userlog`
--
ALTER TABLE `userlog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `ordertrackhistory`
--
ALTER TABLE `ordertrackhistory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `productreviews`
--
ALTER TABLE `productreviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `userlog`
--
ALTER TABLE `userlog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ordertrackhistory`
--
ALTER TABLE `ordertrackhistory`
  ADD CONSTRAINT `ordertrackhistory_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`);

--
-- Constraints for table `productreviews`
--
ALTER TABLE `productreviews`
  ADD CONSTRAINT `productreviews_ibfk_1` FOREIGN KEY (`productId`) REFERENCES `products` (`id`);

--
-- Constraints for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD CONSTRAINT `subcategory_ibfk_1` FOREIGN KEY (`categoryid`) REFERENCES `category` (`id`);

--
-- Constraints for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `wishlist_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
